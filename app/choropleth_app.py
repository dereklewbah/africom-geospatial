from importlib.metadata import SelectableGroups
import dash
import json
import datetime
import numpy as np
import pandas as pd
import plotly.express as px
import dash_core_components as dcc
import dash_html_components as html
from flask_caching import Cache
from dash.dependencies import Input, Output, State

# Define some inputs
DATA_IN = '../data/AFRICOM_EAST_comprehensive.csv'
GEOJSON = '../data/AFRICOM_EAST.geojson'

# Create the Dash application instance
app = dash.Dash(__name__)

# Configure cache
cache = Cache(app.server, config={
    'CACHE_TYPE': 'simple'
})

# Data loading functions
@cache.memoize(timeout=1800)
def load_AFRICOM_data():
    return pd.read_csv(DATA_IN)

@cache.memoize(timeout=1800)
def load_geojson():
    with open(GEOJSON, 'r') as f:
        return json.load(f)

# Data processing function
def filter_and_sum_by_date_range(df, start_date, end_date):
    # Convert the date column to datetime type if it's not already
    if not pd.api.types.is_datetime64_dtype(df['date']):
        df['date'] = pd.to_datetime(df['date'])

    # Filter the DataFrame based on the date range
    filtered_df = df[(df['date'] >= start_date) & (df['date'] <= end_date)]

    # Sum the count columns by province over the date range
    summed_df = filtered_df.groupby('province').agg(
        {'country': 'first', 'district': 'first', 'latitude': 'first', 'longitude': 'first', 'id': 'first',
        'FINTEL': 'sum', 'RAW': 'sum'}).reset_index()

    # Quick edit for ratioed entry
    summed_df['RAW:FINTEL'] = summed_df.apply(lambda row: row.RAW/row.FINTEL if row.FINTEL != 0 else -1, axis=1)

    return summed_df

selectable_columns = {'FINTEL':'FINTEL', 'RAW':'RAW', 'RAW:FINTEL':'RAW:FINTEL'}

# Define the layout of the Dash application.
app.layout = html.Div(style={'display':'flex'}, children=[
    

    html.Div(style={'width': '80%'}, children=[
        dcc.Graph(id='example-graph'),
    ]),
    

    html.Div(style={'width': '30%', 'margin-left': '10px', 'display': 'flex', 'flex-direction': 'column', 'justify-content': 'top'}, children=[

        html.H1(children='AFRICOM EAST Dashboard'),

        html.Div(style={'margin-bottom': '10px', 'width': '350px', 'justify-content':'center'}, children=[
            dcc.DatePickerRange(
                id='date-picker',
                start_date=datetime.date(year=2023, month=6, day=1),
                end_date=datetime.date(year=2023, month=6, day=30)
            )
        ]),

        html.Div(style={'margin-bottom': '10px', 'width': '350px', 'justify-content':'center'}, children=[
            dcc.Dropdown(
                id='dropdown',
                options=[{'label': j, 'value': k} for j, k in selectable_columns.items()],
                value=list(selectable_columns.values())[0]
            ),
        ]),

        html.Div(children=[
            html.Button('Update', id='update-map', n_clicks=0,
                style={'background-color': '#1f77b4', 
                'color': 'white', 
                'border': 'none', 
                'padding': '10px 20px', 
                'text-align': 'center', 
                'text-decoration': 'none', 
                'display': 'inline-block', 
                'font-size': '16px', 
                'margin': '4px 2px', 
                'cursor': 'pointer',
                'justify-content':'center'}
            )
        ]),
    ])
    
])

# Callback function
@app.callback(
    Output('example-graph', 'figure'),
    inputs=Input('update-map', 'n_clicks'),
    state=[State('date-picker', 'start_date'),
     State('date-picker', 'end_date'),
     State('dropdown','value')]
)
def update_figure(n_clicks, start_date, end_date, column):
    df = load_AFRICOM_data()
    geojson = load_geojson()
    df = filter_and_sum_by_date_range(df, start_date, end_date)
    
    # Create the custom color scale
    color_scale = [
        [0, 'red'],        # corresponds to 0 in your data
        [0.5, 'white'],     # corresponds to 1 in your data
        [1, 'blue'],         # corresponds to 20 in your data
    ]
    colors = {'FINTEL':'greens', 'RAW':'blues', 'RAW:FINTEL':color_scale}

    # Defining a figure for the graph
    fig = px.choropleth(df, geojson=geojson, locations='province', 
                        color=df[column] if column != 'RAW:FINTEL' else np.log(df[column]),
                        # color=np.log(df[column]),
                        featureidkey='properties.province',
                        color_continuous_scale=colors[column],
                        range_color=(-1, df[column].quantile(0.85)) if column != 'RAW:FINTEL' else None,
                        hover_name='province',
                        hover_data=['FINTEL', 'RAW']
                )

    fig.update_layout(
            geo = dict(
                lataxis = dict(range=[-12,25]),
                lonaxis = dict(range=[13,52]),
                landcolor = 'rgb(217, 217, 217)',
            ),
            width = 1200,
            height = 800,
        )

    fig.update_geos(
        showcountries=True,
        resolution=50
    )
    return fig

# Run the Dash app
if __name__ == '__main__':
    app.run_server(debug=True)
