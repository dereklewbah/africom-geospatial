coordinates = {
  "Burundi": [29.9189, -3.3731],
  "Comoros": [43.8722, -11.874],
  "Djibouti": [42.5903, 11.8251],
  "Eritrea": [39.7823, 15.1794],
  "Ethiopia": [40.4897, 9.1450],
  "Kenya": [36.8219, -1.2921],
  "Madagascar": [46.8691, -18.7669],
  "Malawi": [34.3015, -13.2543],
  "Mauritius": [57.5522, -20.3484],
  "Mozambique": [35.5296, -18.6657],
  "Reunion": [55.5364, -21.1151],
  "Rwanda": [29.8739, -1.9403],
  "Seychelles": [55.4920, -4.6796],
  "Somalia": [46.1996, 5.1521],
  "Tanzania": [34.8888, -6.3690],
  "Uganda": [32.2903, 1.3733],
  "Zambia": [27.8493, -13.1339],
  "Zimbabwe": [29.1549, -19.0154]
}
